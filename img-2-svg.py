import argparse
from PIL import Image
import potrace

def convert_to_svg(image_path, svg_path):
    # Load the image
    bitmap = Image.open(image_path)

    # Convert the image to grayscale and then to a binary (black & white) image
    bitmap = bitmap.convert('L')
    bitmap = bitmap.point(lambda x: 0 if x < 128 else 255, '1')

    # Create a bitmap object
    bitmap = potrace.Bitmap(bitmap)
    path = bitmap.trace()

    # Export the path to an SVG file
    path.to_svg(svg_path)

    return svg_path

def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Convert an image to SVG format.')
    parser.add_argument('image_path', help='The path to the image file to convert.')
    parser.add_argument('-o', '--output', default='output.svg', help='The output SVG file path.')
    args = parser.parse_args()

    # Convert the image to SVG
    svg_file = convert_to_svg(args.image_path, args.output)
    print(f"SVG file created at: {svg_file}")

if __name__ == '__main__':
    main()