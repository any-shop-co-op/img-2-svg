# Image to SVG Converter

This tool converts raster images (like PNG or JPEG) into scalable vector graphics (SVG) files using Python, Pillow, and the potrace library.

## Prerequisites

Before running the script, ensure that you have Python installed on your system along with the following Python libraries:
- Pillow (PIL Fork)
- python-potrace

You can install the required Python libraries using `pip`:

```bash
pip install Pillow
pip install python-potrace
```

Additionally, you need the potrace binary. On NixOS, you can install it with:

```bash
nix-env -iA nixos.potrace
```

Or on Ubuntu, Debian, or similar systems, use:

```bash
sudo apt-get install potrace
```

## Usage

To use the script, navigate to the script's directory and run the following command:

```bash
python img-2-svg.py <path-to-image> -o <output-svg-path>
```

Replace `<path-to-image>` with the path to the image you want to convert, and `<output-svg-path>` with the path where you want the SVG file to be saved.

For example:

```bash
python img-2-svg.py "~/Pictures/Screenshots/Screenshot.png" -o "~/output.svg"
```

## Contributions

Contributions are welcome! If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Don't forget to give the project a star! Thanks again!

## License

Distributed under the MIT License. See `LICENSE` for more information.