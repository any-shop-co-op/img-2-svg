{ pkgs ? import <nixpkgs> {} }:

let
  pythonEnv = pkgs.python3.withPackages (ps: with ps; [
    pillow
    pip # Include pip only if you need to use pip within the nix-shell
    # If there's a Nix expression for python-potrace, include it here.
    # If not, you might have to create one.
  ]);
in
pkgs.mkShell {
  buildInputs = [
    pythonEnv
    pkgs.potrace
  ];

  # Optionally, if you really need to install python-potrace with pip, and you are sure it won't harm your environment:
  shellHook = ''
    if [ ! -d "$HOME/.local/lib" ]; then
      export PYTHONUSERBASE=$HOME/.local
      pip install --user python-potrace || true
    fi
  '';
}